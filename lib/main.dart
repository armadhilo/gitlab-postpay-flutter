import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:postpaymobile/login_page.dart';
import 'package:postpaymobile/startup_page.dart';
import 'package:postpaymobile/verification/verification_face/verification_face_camera_page.dart';
import 'package:postpaymobile/verification/verification_ktp/verification_ktp_camera_page.dart';
import 'package:postpaymobile/verification/verification_ktp/verification_ktp_page.dart';
import 'package:postpaymobile/verification/verification_menu_page.dart';
import 'package:postpaymobile/verification/verification_selfiektp/verification_selfiektp_camera_page.dart';
import 'package:postpaymobile/verification/verification_video/verification_video_failed_page.dart';
import 'package:postpaymobile/verification/verification_video/verification_video_page.dart';
import 'package:postpaymobile/verification/verification_video/verification_video_succes_page.dart';
import 'package:postpaymobile/widget/base_state.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(MyApp());
  });
}



class MyApp extends StatefulWidget {
  const MyApp({ Key key }) : super(key: key);
  
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends BaseStatefulState{


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Postpay Mobile',
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (routeSettings) {
        switch (routeSettings.name) {
            case '/':
                return MaterialPageRoute(builder: (_) => StartupPage());
            case '/startup':
                return MaterialPageRoute(builder: (_) => StartupPage());
            case '/login_page':
                return MaterialPageRoute(builder: (_) => LoginPage());
            case '/verif_menu':
                return MaterialPageRoute(builder: (_) => VerificationMenuPage(step: routeSettings.arguments));
            case '/verif_face_camera_page':
                return MaterialPageRoute(builder: (_) => VerificationFaceCameraPage());
            case '/verif_ktp_camera_page':
                return MaterialPageRoute(builder: (_) => VerificationKtpCameraPage(from: routeSettings.arguments));
            case '/verif_ktp_page':
                return MaterialPageRoute(builder: (_) => VerificationKtpPage(filektp: routeSettings.arguments));
             case '/verif_selfiektp_camera_page':
                return MaterialPageRoute(builder: (_) => VerificationSelfieKtpCameraPage());
            case '/verif_video_page':
                return MaterialPageRoute(builder: (_) => VerificationVideoPage());
            case '/verif_video_succes':
                return MaterialPageRoute(builder: (_) => VerificationVideoSuccesPage(file: routeSettings.arguments));
            case '/verif_video_failed':
                return MaterialPageRoute(builder: (_) => VerificationVideoFailedPage());
            default:
                return MaterialPageRoute(builder: (_) => StartupPage());
        }
      },
    );
  }



  
}

