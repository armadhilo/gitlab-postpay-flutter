import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:postpaymobile/utility/theme.dart';
import 'package:postpaymobile/verification/verification_menu_page.dart';
import 'package:postpaymobile/widget/base_button.dart';
import 'package:postpaymobile/widget/base_state.dart';
import 'package:postpaymobile/widget/base_textfield.dart';

class LoginPage extends StatefulWidget {
  static const String routes = '/login_page';

  final int step;

  LoginPage({this.step});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends BaseStatefulState<LoginPage> {
  

  @override
  void initState() {

    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        backgroundColor: Colors.grey[100],
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [

                    SizedBox(height: setHeight(0.075)),
                    Container(
                      alignment: Alignment.center,
                      child: Image.asset("assets/ic_icon.png", scale: 2,)),

                    Container(
                      margin: EdgeInsets.symmetric(vertical: 5),
                      child: Text("Hai, Apa Kabar Hari Ini?👋🏻", style: black18wBold)
                    ),

                    Container(
                      margin: EdgeInsets.symmetric(vertical: 5),
                      child: Text("Yuk, masuk dan bayar apapun disini.", style: black14w400)
                    ),

                    SizedBox(height: setHeight(0.05)),

                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: BaseInputTextfield(
                        // controller: ,
                        textTitle: "Username",
                        textHint: "Masukkan username",
                        type: TextInputType.text,
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: BaseInputTextFieldPassword(
                        // controller: ,
                        textTitle: "Password",
                        textHint: "Masukkan password",
                        type: TextInputType.text,
                      ),
                    ),

                    Container(
                      alignment: Alignment.centerRight,
                      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      child: Text("Lupa Password", style: orange14w700)
                    ),


                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      child: Row(
                        children: [
                          Expanded(
                            child: BaseButtonNextColor(
                              colorBotton: orangeColor,
                              colorText: Colors.white,
                              textTitle: "Login",
                              onpressed: (){
                                Navigator.pushNamedAndRemoveUntil(context, VerificationMenuPage.routes, (route) => false, arguments: 1);
                              },
                            ),
                          ),
                          SizedBox(width: 5),
                          Icon(Icons.fingerprint, color: orangeColor, size: 45),
                        ],
                      ),
                    ),


                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Belum Punya Akun? ", style: black14w400),
                          Text("Daftar Di sini", style: orange14w700),
                        ],
                      )
                    ),
                  ],
                ),
              ),


              Align(
                alignment: Alignment.centerRight,
                child: Container(
                  height: 50,
                  width: 50,
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/ic_pedulilindungi.png"), fit: BoxFit.cover),
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                  ),
                ),
              )

            ],
          ),
        ),
      ),
    );
  }


}