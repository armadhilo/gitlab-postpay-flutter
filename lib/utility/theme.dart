import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';

Color redColor = Color(0xFFC9252C);
Color redLightColor = Color(0xFFFAEBEC);
Color darkRedColor = Color(0xFF6E0001);
Color accentColor = Color(0xFFE0E1E1);
Color orangeColor = Color(0xFFF3722F);
Color orangeLightColor = Color(0xFFfbe8db);

Color britishBlue1 = Color(0xFF00a8ff);
Color britishBlue2 = Color(0xFF0097e6);
Color britishRed1 = Color(0xFFe84118);
Color britishRed2 = Color(0xFFc23616);
Color britishPurple1 = Color(0xFF9c88ff);
Color britishPurple2 = Color(0xFF8c7ae6);
Color britishWhite1 = Color(0xFFf5f6fa);
Color britishWhite2 = Color(0xFFdcdde1);
Color britishYellow1 = Color(0xFFfbc531);
Color britishYellow2 = Color(0xFFe1b12c);
Color britishYellow3 = Color(0xFFFFFEF5);
Color britishGrey1 = Color(0xFF7f8fa6);
Color britishGrey2 = Color(0xFF718093);
Color britishGreen1 = Color(0xFF4cd137);
Color britishGreen2 = Color(0xFF44bd32);
Color britishBlue31 = Color(0xFF273c75);
Color britishBlue32 = Color(0xFF192a56);
Color britishBlue21 = Color(0xFF487eb0);
Color britishBlue22 = Color(0xFF40739e);
Color britishGrey21 = Color(0xFF353b48);
Color britishGrey22 = Color(0xFF2f3640);

const String fontInter = 'Inter';
const String fontInterBlack = 'Inter-Black';
const String fontInterReguler = 'Inter-Regular';
const String fontInterMedium = 'Inter-Medium';
const String fontInterSemiBold = 'Inter-SemiBold';
const String fontInterBold = 'Inter-Bold';
const String fontInterThin = 'Inter-Thin';
const String fontInterLight = 'Inter-Thin';

const double textSize36 = 36.0;
const double textSize24 = 24.0;
const double textSize16 = 16.0;
const double textSize14 = 14.0;
const double textSize12 = 12.0;
const double textSize11 = 11.0;
const double textSize10 = 10.0;

//w400
TextStyle black12w400 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w400,
  fontSize: 12,
  color: Colors.black,
);

  TextStyle black14w400 = TextStyle(
    fontFamily: fontInter,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    color: Colors.black,
  );

TextStyle black15w400 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w400,
  fontSize: 15,
  color: Colors.black,
);

TextStyle black16w400 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: Colors.black,
);

TextStyle grey12w400 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w400,
  fontSize: 12,
  color: Colors.grey,
);

TextStyle grey16w400 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: Colors.grey[600],
);

TextStyle white16w400 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: Colors.white,
);

TextStyle yellow12w400 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w400,
  fontSize: 12,
  color: Colors.yellow,
);

//w500
TextStyle black12w500 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w500,
  fontSize: 12,
  color: Colors.black,
);

TextStyle black14w500 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w500,
  fontSize: 14,
  color: Colors.black,
);

TextStyle black16w500 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w500,
  fontSize: 16,
  color: Colors.black,
);

TextStyle white14w500 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w500,
  fontSize: 14,
  color: Colors.white,
);

TextStyle red12w500 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w500,
  fontSize: 12,
  color: Colors.red,
);

TextStyle red14w500 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w500,
  fontSize: 14,
  color: Colors.red,
);

//w700
TextStyle orange14w700 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w700,
  fontSize: 14,
  color: orangeColor,
);

TextStyle black14w700 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w700,
  fontSize: 14,
  color: Colors.black,
);

TextStyle black16w700 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w700,
  fontSize: 16,
  color: Colors.black,
);

TextStyle black18w700 = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.w700,
  fontSize: 18,
  color: Colors.black,
);

//bold
TextStyle black14wBold = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.bold,
  fontSize: 14,
  color: Colors.black,
);

TextStyle black16wBold = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.bold,
  fontSize: 16,
  color: Colors.black,
);

TextStyle black18wBold = TextStyle(
  fontFamily: fontInter,
  fontWeight: FontWeight.bold,
  fontSize: 18,
  color: Colors.black,
);