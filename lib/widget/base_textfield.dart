import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:postpaymobile/utility/theme.dart';

class BaseInputTextfield extends StatelessWidget {
  const BaseInputTextfield({Key key, this.textTitle, this.textHint, this.type, this.controller, this.paddingTop = 5, 
  this.paddingBottom = 5, this.change = true, this.maxLength = 100, this.readOnly = false})
      : super(key: key);
  final String textTitle;
  final String textHint;
  final TextInputType type;
  final TextEditingController controller;
  final double paddingTop;
  final double paddingBottom;
  final bool change;
  final int maxLength;
  final bool readOnly;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: paddingTop, bottom: paddingBottom),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            textTitle,
            style: black14w400,
          ),
          Container(
            height: 40,
            margin: EdgeInsets.symmetric(vertical: 10),
            child: TextField(
              maxLength: maxLength,
              enabled: change,
              keyboardType: type,
              controller: controller,
              style: black14w500,
              readOnly: readOnly,
              decoration: InputDecoration(
                  counterText: '',
                  contentPadding: EdgeInsets.only(left: 15,bottom: 5),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    borderSide: BorderSide(width: 1, color: orangeColor),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  fillColor: Colors.white,
                  filled: true,
                  hintText: textHint,
                  hintStyle: grey16w400),
            ),
          ),
        ],
      ),
    );
  }
}



class BaseInputTextFieldPassword extends StatefulWidget {
   const BaseInputTextFieldPassword({Key key, this.textTitle, this.textHint, this.type, this.controller, this.maxLength})
      : super(key: key);
  final String textTitle;
  final String textHint;
  final TextInputType type;
  final TextEditingController controller;
  final int maxLength;

  @override
  _BaseInputTextFieldPasswordState createState() => _BaseInputTextFieldPasswordState();
}

class _BaseInputTextFieldPasswordState extends State<BaseInputTextFieldPassword> {
  bool showhide = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            widget.textTitle,
            style: black14w400,
          ),
          Container(
            height: 40,
            margin: EdgeInsets.symmetric(vertical: 5),
            child: TextField(
              keyboardType: widget.type,
              controller: widget.controller,
              maxLength: widget.maxLength,
              obscureText: showhide,
              style: TextStyle(
                color: Colors.black,
                fontSize: 12,
                fontFamily: 'OpenSans',
              ),
              decoration: InputDecoration(
                  suffixIcon: InkWell(
                    onTap: () {
                      setState(() {
                        showhide = ! showhide;
                      });
                    },
                    child: Icon( showhide ? Icons.visibility_off : Icons.visibility, color: Colors.grey,)),
                  counterText: '',
                  contentPadding: EdgeInsets.only(left: 15,bottom: 5),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    borderSide: BorderSide(width: 1, color: orangeColor),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  fillColor: Colors.white,
                  filled: true,
                  hintText: widget.textHint,
                  hintStyle: grey16w400),
            ),
          ),
        ],
      ),
    );
  }
}

