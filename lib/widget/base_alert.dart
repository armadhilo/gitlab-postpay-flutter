import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:postpaymobile/startup_page.dart';
import 'package:postpaymobile/utility/theme.dart';

showAlertMessageSettings(BuildContext context, String header, String message, String textButton) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: ()async=>false,
          child: Dialog(
            shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0)), //this right here
            child: Container(
              height: 220,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: Text(header,
                          style: black16w500, textAlign: TextAlign.center),
                    ),
                    Text(message,
                        style: black14w500, textAlign: TextAlign.center),
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                                Navigator.pushNamedAndRemoveUntil(context, "/", (route) => false, arguments: 1);
                              },
                              color: Colors.green,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Container(
                                height: 30,
                                width: 80,
                                child: Center(
                                  child: Text(
                                    "Coba Lagi",
                                    style: white14w500,
                                  ),
                                )),
                            ),
                            SizedBox(width: 10),
                            FlatButton(
                              onPressed: () async {
                                await  openAppSettings();
                              },
                              color: Colors.orange,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Container(
                                height: 30,
                                width: 80,
                                child: Center(
                                  child: Text(
                                    textButton,
                                    style: white14w500,
                                  ),
                                )),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      });
  return CircularProgressIndicator();
}

showAlertMessage(BuildContext context, String header, String message, String textButton) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0)), //this right here
          child: Container(
            height: 220,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: Text(header,
                        style: black16w500, textAlign: TextAlign.center),
                  ),
                  Text(message,
                      style: black14w500, textAlign: TextAlign.center),
                  Expanded(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        color: Colors.orange,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Container(
                          height: 30,
                          width: 100,
                          child: Center(
                            child: Text(
                              textButton,
                              style: white14w500,
                            ),
                          )),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      });
  return CircularProgressIndicator();
}
