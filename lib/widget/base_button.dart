import 'package:flutter/material.dart';

class BaseButtonNextColor extends StatelessWidget {
  const BaseButtonNextColor({Key key, this.height = 48 , this.textTitle, this.onpressed, this.colorText, this.colorBotton})
      : super(key: key);
  final String textTitle;
  final Function onpressed;
  final Color colorBotton;
  final Color colorText;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      // decoration: BoxDecoration(
          // color: colorBotton,
          // borderRadius: BorderRadius.all(Radius.circular(5))
      // ),
      child: Material(
        color: colorBotton,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        child: InkWell(
          onTap: () {
            onpressed();
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  textTitle,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400, color: colorText),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

