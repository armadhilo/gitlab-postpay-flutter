import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:postpaymobile/utility/theme.dart';

abstract class BaseStatefulState<T extends StatefulWidget> extends State<T> {
  BaseStatefulState() {
    print('Initial Base State');
  }

  @override
  void initState() {
    super.initState();
  }

  handleDismis() {
    Navigator.of(context).pop();
  }

  double setWidth(double sizeWidth) {
    Size size = MediaQuery.of(context).size;
    return size.width * sizeWidth;
  }

  double setHeight(double sizeHeight) {
    Size size = MediaQuery.of(context).size;
    return size.height * sizeHeight;
  }

  void progressDialogFull(BuildContext context) {
    showDialog(
      barrierColor: Colors.black.withOpacity(0.5),
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.transparent,
          insetPadding: EdgeInsets.symmetric(horizontal: setWidth(0.25)),
          elevation: 0,
          content: Container(
            height: 100,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            child: SpinKitRing(
              color: orangeColor,
              size: 50.0,
            ),
          ),
        );
      },
    );
  }
}
