import 'dart:io';

import 'package:flutter/material.dart';
import 'package:postpaymobile/utility/theme.dart';
import 'package:postpaymobile/verification/verification_ktp/verification_ktp_camera_page.dart';
import 'package:postpaymobile/verification/verification_menu_page.dart';
import 'package:postpaymobile/widget/base_button.dart';
import 'package:postpaymobile/widget/base_state.dart';
import 'package:postpaymobile/widget/base_textfield.dart';

class VerificationKtpPage extends StatefulWidget {
  static const String routes = '/verif_ktp_page';

  final File filektp;

  VerificationKtpPage({this.filektp});

  @override
  State<VerificationKtpPage> createState() => _VerificationKtpPageState();
}

class _VerificationKtpPageState extends BaseStatefulState<VerificationKtpPage> {
  TextEditingController noId = TextEditingController();
  TextEditingController nameId = TextEditingController();
  TextEditingController nationalId = TextEditingController();

  int step = 2;
  File imageKtp;

  void getInitKtp(){
    noId.text = "1023102371739231";
    nameId.text = "Arjun Prayoga Aji";
    nationalId.text = "Indonesia";

    imageKtp = widget.filektp;
  }

  //disini bisa tembakkan ke web service, dan ketika sukses ke halaman ini saja untuk lanjut step 3
  toUploadVerificationKtp(){
    Navigator.pushNamedAndRemoveUntil(context, VerificationMenuPage.routes, (route) => false, arguments: 3);
  }

  @override
  void initState(){
    getInitKtp();

    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        Navigator.pushNamedAndRemoveUntil(context, VerificationMenuPage.routes, (route) => false, arguments: 2); //back step2
        return;
      },
      child: GestureDetector(
        onTap: (){
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
          backgroundColor: Colors.grey[100],
          resizeToAvoidBottomInset: false,
          body: SafeArea(
            child: Column(
              children: [

                widgetStepIndicator(),

                verificationKtpPage(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //////////////////////////////////////////////////////////////widget step indicator
  Widget widgetStepIndicator(){
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 10,
              decoration: BoxDecoration(
                  color: (step >= 1) ? orangeColor : Colors.grey[300],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    bottomLeft: Radius.circular(30)
                  ),
              ),
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            child: Container(
              height: 10,
              color: (step >= 2) ? orangeColor : Colors.grey[300],
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            child: Container(
              height: 10,
              color: (step >= 3) ? orangeColor : Colors.grey[300],
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            child: Container(
              height: 10,
              decoration: BoxDecoration(
                  color: (step >= 4) ? orangeColor : Colors.grey[300],
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    bottomRight: Radius.circular(30)
                  ),
              ),
            ),
          ),
          SizedBox(width: 10),
          Text("${step.toString()}/4", style: black16w700)
        ],
      ),
    );
  }



  ///////////////////////////////////////////////////////////////////body page verification ktp
  Widget verificationKtpPage(){
    return Expanded(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: [

            SizedBox(height: setHeight(0.035)),

            Expanded(
              child: Container(
                child: ListView(
                  physics: BouncingScrollPhysics(),
                  children: [
                    
                    //section 1
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Cek Dokumenmu", style: black18w700),
                        SizedBox(height: setHeight(0.0175)),
                        Text("Pastikan data yang ada sudah benar", style: grey16w400, textAlign: TextAlign.center),
                        SizedBox(height: setHeight(0.0175)),
                      ],
                    ),

                    //section 2
                    // Image.asset("assets/ic_ktpdummy.png", scale: 1.85),
                    (imageKtp != null) ? 
                    Container(
                      height: setHeight(0.4),
                      child: Image.file(imageKtp, fit: BoxFit.cover)) 
                    : Container(
                      child: Text("- Sedang memuat foto ktp, silakan RETAKE jika belum tampil -", style: grey12w400)),
                    SizedBox(height: setHeight(0.015)),

                    BaseButtonNextColor(
                      textTitle: "Retake Foto",
                      colorBotton: orangeLightColor,
                      colorText: orangeColor,
                      onpressed: () async {

                        FocusScope.of(context).requestFocus(new FocusNode());
                        progressDialogFull(context);
                        Future.delayed(Duration(milliseconds: 700), () async {
                          handleDismis();
                          dynamic file = await Navigator.pushNamed(context, VerificationKtpCameraPage.routes, arguments: "RETAKE");

                          if(file != null){
                            setState(() {
                              imageKtp = file;
                            });
                          }
                        });

                      },
                    ),


                    SizedBox(height: 15),
                    BaseInputTextfield(
                      controller: noId,
                      textTitle: "Nomor KTP",
                      textHint: "Masukkan nomor KTP",
                      type: TextInputType.number,
                      maxLength: 16,
                      paddingBottom: 0,
                      paddingTop: 0,
                    ),

                    SizedBox(height: 15),
                    BaseInputTextfield(
                      controller: nameId,
                      textTitle: "Nama Sesuai KTP",
                      textHint: "Masukkan nama sesuai KTP",
                      type: TextInputType.text,
                      maxLength: 255,
                      paddingBottom: 0,
                      paddingTop: 0,
                    ),

                    SizedBox(height: 15),
                    BaseInputTextfield(
                      controller: nationalId,
                      textTitle: "Kewarnegaraan",
                      textHint: "Masukkan kewarnegaraan",
                      type: TextInputType.text,
                      maxLength: 255,
                      paddingBottom: 0,
                      paddingTop: 0,
                    ),

                    SizedBox(height: setHeight(0.5)),
                  ],
                ),
              ),
            ),

            //section 4
            Container(
              alignment: Alignment.center,
              child: BaseButtonNextColor(
                height: setHeight(0.06),
                textTitle: "Lanjut Verifikasi",
                colorBotton: orangeColor,
                colorText: Colors.white,
                onpressed: (){
                  toUploadVerificationKtp();
                },
              ),
            ),

            SizedBox(height: setHeight(0.035)),

          ],
        ),
      ),
    );
  }


}