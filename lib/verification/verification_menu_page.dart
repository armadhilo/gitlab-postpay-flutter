import 'package:flutter/material.dart';
import 'package:postpaymobile/utility/theme.dart';
import 'package:postpaymobile/verification/verification_face/verification_face_camera_page.dart';
import 'package:postpaymobile/verification/verification_ktp/verification_ktp_camera_page.dart';
import 'package:postpaymobile/verification/verification_selfiektp/verification_selfiektp_camera_page.dart';
import 'package:postpaymobile/verification/verification_video/verification_video_page.dart';
import 'package:postpaymobile/widget/base_button.dart';
import 'package:postpaymobile/widget/base_state.dart';

class VerificationMenuPage extends StatefulWidget {
  static const String routes = '/verif_menu';

  final int step;

  VerificationMenuPage({this.step});

  @override
  State<VerificationMenuPage> createState() => _VerificationMenuPageState();
}

class _VerificationMenuPageState extends BaseStatefulState<VerificationMenuPage> {
  int step = 1;

  //disini inisialisasi halaman awal, parsing argumen sebelum kesini adalah 1, tampung divariabel step
  void getInit(){
    setState(() {
      step = widget.step;
    });
  }

  //opsional buat clear hiraukan
  void reset(){
    setState(() {
      step = 1;
    });
  }

  //ketika verifikasi wajah sukses buat counter step menjadi = 2 //harusnya ke halaman verifikasi selfie
  toVerificationFace(){
    setState(() {
      // step = 2;
      Navigator.pushNamed(context, VerificationFaceCameraPage.routes);
    });
  }

  //ketika step verifikasi ktp, arahkan helaman kamera, terus verificationktp.routes, lalu jika sukses arahkan ke halaman ini parsing argumen = 3 utk step selanjutnya
  toVerificationKtp(){
    setState(() {
      // step = 3;
      Navigator.pushNamed(context, VerificationKtpCameraPage.routes, arguments: "NEW");
    });
  }

  //ketika verifikasi wajah diri buat counter step menjadi = 4 //harusnya ke halaman verifikasi diri
  toVerificationDiri(){
    setState(() {
      // step = 4;
      Navigator.pushNamed(context, VerificationSelfieKtpCameraPage.routes);
    });
  }

  //ketika verifikasi video
  toVerificationVideo(){
    setState(() {
      // step = 5;
      Navigator.pushNamed(context, VerificationVideoPage.routes);
    });
  }

  @override
  void initState() {
    getInit();

    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: SafeArea(
        child: Column(
          children: [

            widgetStepIndicator(),

            (step == 1) ? verificationFacePage() : Container(),

            (step == 2) ? verificationKtpPage() : Container(),

            (step == 3) ? verificationDiriKtpPage() : Container(),

            (step == 4) ? verificationVideoPage() : Container(),
          ],
        ),
      ),
    );
  }

  //////////////////////////////////////////////////////////////widget step indicator
  Widget widgetStepIndicator(){
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 10,
              decoration: BoxDecoration(
                  color: (step >= 1) ? orangeColor : Colors.grey[300],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    bottomLeft: Radius.circular(30)
                  ),
              ),
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            child: Container(
              height: 10,
              color: (step >= 2) ? orangeColor : Colors.grey[300],
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            child: Container(
              height: 10,
              color: (step >= 3) ? orangeColor : Colors.grey[300],
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            child: Container(
              height: 10,
              decoration: BoxDecoration(
                  color: (step >= 4) ? orangeColor : Colors.grey[300],
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    bottomRight: Radius.circular(30)
                  ),
              ),
            ),
          ),
          SizedBox(width: 10),
          Text("${step.toString()}/4", style: black16w700)
        ],
      ),
    );
  }



  ///////////////////////////////////////////////////////////////////body page verification wajah
  Widget verificationFacePage(){
    return Expanded(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: [

            SizedBox(height: setHeight(0.035)),

            Expanded(
              child: Container(
                child: Column(
                  children: [

                    //section 1
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text("Verifikasi Wajah", style: black18w700),
                        SizedBox(height: setHeight(0.0175)),
                        Text("Untuk verifikasi wajah kamu, baca ketentuan\nberikut :", style: grey16w400, textAlign: TextAlign.center),
                        SizedBox(height: setHeight(0.0175)),
                      ],
                    ),

                    //section 2
                    Image.asset("assets/ic_selfie.png", scale: 2),
                    SizedBox(height: setHeight(0.0175)),


                    //section 3
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[200],
                            spreadRadius: 2,
                            blurRadius: 5,
                            offset: Offset(0, 3),
                          ),
                        ],
                        borderRadius: BorderRadius.all(Radius.circular(20)
                        ),
                      ),
                      child: Column(
                        children: [

                          SizedBox(height: setHeight(0.03)),

                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("1", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Pastikan ', style: black16w400),
                                      TextSpan(text: 'kamera tidak blur ', style: black16wBold),
                                      TextSpan(text: 'dan hanya ada ', style: black16w400),
                                      TextSpan(text: 'satu wajah ', style: black16wBold),
                                      TextSpan(text: 'dalam kamera ', style: black16w400),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: setHeight(0.015)),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("2", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Pastikan ', style: black16w400),
                                      TextSpan(text: 'wajah jelas ', style: black16wBold),
                                      TextSpan(text: 'saat pengambilan ', style: black16w400)
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),


                          SizedBox(height: setHeight(0.015)),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("3", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Wajah yang diambil adalah ', style: black16w400),
                                      TextSpan(text: 'wajah sendiri ', style: black16wBold),
                                      TextSpan(text: 'bukan orang lain ', style: black16w400)
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: setHeight(0.03)),

                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),


            //section 4
            Container(
              alignment: Alignment.center,
              child: BaseButtonNextColor(
                height: setHeight(0.06),
                textTitle: "Mulai Verifikasi Wajah",
                colorBotton: orangeColor,
                colorText: Colors.white,
                onpressed: (){
                  toVerificationFace();
                },
              ),
            ),

            SizedBox(height: setHeight(0.035)),

          ],
        ),
      ),
    );
  }




  ///////////////////////////////////////////////////////////////////body page verification ktp
  Widget verificationKtpPage(){
    return Expanded(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: [

            SizedBox(height: setHeight(0.035)),

            Expanded(
              child: Container(
                child: Column(
                  children: [
                    
                    //section 1
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Verifikasi KTP", style: black18w700),
                        SizedBox(height: setHeight(0.0175)),
                        Text("Foto KTP kamu dengan baca keterntuan berikut :", style: grey16w400, textAlign: TextAlign.center),
                        SizedBox(height: setHeight(0.0175)),
                      ],
                    ),

                    //section 2
                    Image.asset("assets/ic_ktp.png", scale: 2),
                    SizedBox(height: setHeight(0.0175)),


                    //section 3
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[200],
                            spreadRadius: 2,
                            blurRadius: 5,
                            offset: Offset(0, 3),
                          ),
                        ],
                        borderRadius: BorderRadius.all(Radius.circular(20)
                        ),
                      ),
                      child: Column(
                        children: [

                          SizedBox(height: setHeight(0.03)),

                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("1", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Pastikan sudah menggunakan ', style: black16w400),
                                      TextSpan(text: 'E-KTP', style: black16wBold),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: setHeight(0.015)),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("2", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Pastikan hasil foto ', style: black16w400),
                                      TextSpan(text: 'jelas, tidak buram, tidak terpotong atau tertutupi ', style: black16wBold),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: setHeight(0.015)),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("3", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Foto yang diupload merupakan foto asli ', style: black16w400),
                                      TextSpan(text: 'bukan hasil scan atau fotocopy ', style: black16wBold),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: setHeight(0.03)),

                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

            //section 4
            Container(
              alignment: Alignment.center,
              child: BaseButtonNextColor(
                height: setHeight(0.06),
                textTitle: "Mulai Verifikasi KTP",
                colorBotton: orangeColor,
                colorText: Colors.white,
                onpressed: (){
                  toVerificationKtp();
                },
              ),
            ),

            SizedBox(height: setHeight(0.035)),

          ],
        ),
      ),
    );
  }




  ///////////////////////////////////////////////////////////////////body page verification diri ktp
  Widget verificationDiriKtpPage(){
    return Expanded(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: [

            SizedBox(height: setHeight(0.035)),

            Expanded(
              child: Container(
                child: Column(
                  children: [
                    
                    //section 1
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Verifikasi Diri & KTP", style: black18w700),
                        SizedBox(height: setHeight(0.0175)),
                        Text("Foto selfie dan KTP kamu dengan baca keterntuan berikut :", style: grey16w400, textAlign: TextAlign.center),
                        SizedBox(height: setHeight(0.0175)),
                      ],
                    ),

                    //section 2
                    Image.asset("assets/ic_selfiektp.png", scale: 1.5),
                    SizedBox(height: setHeight(0.0175)),


                    //section 3
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[200],
                            spreadRadius: 2,
                            blurRadius: 5,
                            offset: Offset(0, 3),
                          ),
                        ],
                        borderRadius: BorderRadius.all(Radius.circular(20)
                        ),
                      ),
                      child: Column(
                        children: [

                          SizedBox(height: setHeight(0.03)),

                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("1", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Pegang KTP dibawah dagu, pastikan  ', style: black16w400),
                                      TextSpan(text: 'tidak menutupi wajah', style: black16wBold),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: setHeight(0.015)),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("2", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Ambil foto dekat dengan kamera, pastikan  ', style: black16w400),
                                      TextSpan(text: 'jelas, tidak buram, tidak terpotong. ', style: black16wBold),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: setHeight(0.03)),

                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

            //section 4
            Container(
              alignment: Alignment.center,
              child: BaseButtonNextColor(
                height: setHeight(0.06),
                textTitle: "Mulai Verifikasi",
                colorBotton: orangeColor,
                colorText: Colors.white,
                onpressed: (){
                  toVerificationDiri();
                },
              ),
            ),

            SizedBox(height: setHeight(0.035)),

          ],
        ),
      ),
    );
  }


  ///////////////////////////////////////////////////////////////////body page verification diri ktp
  Widget verificationVideoPage(){
    return Expanded(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: [

            SizedBox(height: setHeight(0.035)),

            Expanded(
              child: Container(
                child: Column(
                  children: [
                    
                    //section 1
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Verifikasi Video", style: black18w700),
                        SizedBox(height: setHeight(0.0175)),
                        Text("Ini adalah verifikasi lanjutan untuk mengetahui kamu lebih jauh", style: grey16w400, textAlign: TextAlign.center),
                        SizedBox(height: setHeight(0.0175)),
                      ],
                    ),

                    //section 2
                    Image.asset("assets/ic_video.png", scale: 1.5),
                    SizedBox(height: setHeight(0.0175)),


                    //section 3
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[200],
                            spreadRadius: 2,
                            blurRadius: 5,
                            offset: Offset(0, 3),
                          ),
                        ],
                        borderRadius: BorderRadius.all(Radius.circular(20)
                        ),
                      ),
                      child: Column(
                        children: [

                          SizedBox(height: setHeight(0.03)),

                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("1", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Pegang ', style: black16w400),
                                      TextSpan(text: 'kamera tidak blur ', style: black16wBold),
                                      TextSpan(text: 'dan hanya ada ', style: black16w400),
                                      TextSpan(text: 'satu wajah ', style: black16wBold),
                                      TextSpan(text: 'dalam kamera ', style: black16w400),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),


                          SizedBox(height: setHeight(0.015)),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("2", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Pastikan  ', style: black16w400),
                                      TextSpan(text: 'wajah jelas ', style: black16wBold),
                                      TextSpan(text: 'saat pengambilan  ', style: black16w400),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: setHeight(0.015)),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("3", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Wajah yang diambil adalah  ', style: black16w400),
                                      TextSpan(text: 'wajah sendiri ', style: black16wBold),
                                      TextSpan(text: 'bukan orang lain', style: black16w400),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: setHeight(0.015)),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 12.5, vertical: 7.5),
                                margin: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                  color: orangeLightColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                                ),
                                child: Text("4", style: red14w500),
                              ),
                              Flexible(
                                child: RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(text: 'Kami akan ', style: black16w400),
                                      TextSpan(text: 'mengambil video ', style: black16wBold),
                                      TextSpan(text: 'dalam beberapa detik', style: black16w400),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: setHeight(0.03)),

                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

            //section 4
            Container(
              alignment: Alignment.center,
              child: BaseButtonNextColor(
                height: setHeight(0.06),
                textTitle: "Mulai Verifikasi",
                colorBotton: orangeColor,
                colorText: Colors.white,
                onpressed: (){
                  toVerificationVideo();
                },
              ),
            ),

            SizedBox(height: setHeight(0.035)),

          ],
        ),
      ),
    );
  }


}