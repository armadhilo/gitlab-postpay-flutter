import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:postpaymobile/utility/theme.dart';
import 'package:postpaymobile/verification/verification_menu_page.dart';
import 'package:postpaymobile/widget/base_alert.dart';
import 'package:postpaymobile/widget/base_state.dart';

class VerificationFaceCameraPage extends StatefulWidget {
  static const String routes = '/verif_face_camera_page';

  @override
  _VerificationFaceCameraPageState createState() => _VerificationFaceCameraPageState();
}

class _VerificationFaceCameraPageState extends BaseStatefulState<VerificationFaceCameraPage> {
  CameraController controller;
  bool flash = false;

  int cameraFlip = 1;
  String message = "Pastikan Wajah kamu berada dalam bingkai dibawah.";

  Future<void> initializeCamera() async {
    var cameras = await availableCameras();
    controller = CameraController(cameras[cameraFlip], ResolutionPreset.medium);
    await controller.initialize();
  }

  toChangeCamera(){
    setState(() {
      if(cameraFlip == 0){
        cameraFlip = 1;
      } else {
        cameraFlip = 0;
      }
    });
  }

  toFlash(){
    // setState(() {
      if(flash){
        controller.setFlashMode(FlashMode.off);
        flash = false;
      } else {
        controller.setFlashMode(FlashMode.torch);
        flash = true;
      }
    // });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Future<File> takePicture() async {
   XFile xFile;

    try {

      //karena flash auto jika keadaan gelap, jadi main sendiri
      if(!flash){
        controller.setFlashMode(FlashMode.off);
      }
      
      xFile = await controller.takePicture();
    } catch (e) {
      return null;
    }

    return File(xFile.path);
  }

  void getTakePicture() async {
    if (!controller.value.isTakingPicture) {
      File result = await takePicture();
      if (result == null){
        handleDismis();
        showAlertMessage(context, "Pesan", "Pengambilan foto gagal, silakan ulangi kembali", "TUTUP");
      } else {

        //kembalikan flash dalam keadaan off setelah berhasil take
        // flash = false;
        // controller.setFlashMode(FlashMode.off);

        Navigator.pushNamedAndRemoveUntil(context, VerificationMenuPage.routes, (route) => false, arguments: 2).then((value) => handleDismis());
      }
    }
  }


  

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: FutureBuilder(
          future: initializeCamera(),
          builder: (_, snapshot) =>
          (snapshot.connectionState == ConnectionState.done) ? 
            Stack(
              children: [
                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.symmetric(vertical: setHeight(0.015), horizontal: setWidth(0.075)),
                      child: InkWell(
                        onTap: (){
                          toFlash();
                        },
                        child: Icon(Icons.flash_on, color: Colors.white)),
                    ),
                    Stack(
                      children: [
                        Container(
                          height: size.width / controller.value.aspectRatio,
                          width: size.width,
                          child: CameraPreview(controller),
                        ),
                        Image.asset('assets/ic_bg_video.png',
                          fit: BoxFit.cover,
                        ),
                        Container(
                          height: setHeight(0.075),
                          color: Colors.grey[700],
                          alignment: Alignment.center,
                          child: Text(message.toString(), style: white14w500, textAlign: TextAlign.center),
                        )
                      ],
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: setHeight(0.2),
                    alignment: Alignment.center,
                    color: Colors.black,
                    padding: EdgeInsets.symmetric(horizontal: setWidth(0.1)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: (){
                              handleDismis();
                            },
                            child: Text("Cancel", style: white16w400)),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("PHOTO", style: yellow12w400),
                            SizedBox(height:5),
                            Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: (){
                                  progressDialogFull(context);
                                  getTakePicture();
                                },
                                child: Image.asset("assets/ic_take.png", scale: 2)),
                            ),
                            SizedBox(height:setHeight(0.025)),
                          ],
                        ),
                        InkWell(
                          onTap: (){
                            toChangeCamera();
                          },
                          child: Icon(Icons.flip_camera_ios_outlined, color: Colors.white, size: 40,))
                      ],
                    ))
                ),
              ],
            )
          : Center(
          child: SizedBox(
            height: 20,
            width: 20,
            child: CircularProgressIndicator(),
          ),
        )),
      ),
    );
  }

}
