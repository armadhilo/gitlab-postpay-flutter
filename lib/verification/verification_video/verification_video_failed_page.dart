import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:postpaymobile/utility/theme.dart';
import 'package:postpaymobile/verification/verification_menu_page.dart';
import 'package:postpaymobile/widget/base_button.dart';
import 'package:postpaymobile/widget/base_state.dart';

class VerificationVideoFailedPage extends StatefulWidget {
  static const String routes = '/verif_video_failed';

  final XFile file;

  VerificationVideoFailedPage({this.file});

  @override
  State<VerificationVideoFailedPage> createState() => _VerificationVideoFailedPageState();
}

class _VerificationVideoFailedPageState extends BaseStatefulState<VerificationVideoFailedPage> {
 
  @override
  void initState() {
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        handleDismis();
        handleDismis();
        return;
      },
      child: Scaffold(
        backgroundColor: Colors.grey[100],
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              SizedBox(height: setHeight(0.035)),

              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.cancel, color: Colors.red[700], size: setHeight(0.2)),

                    SizedBox(height: setHeight(0.0175)),
                    Text("Verifikasi Gagal!!", style: black18w700),
                    SizedBox(height: setHeight(0.0175)),
                    Text("Silakan ulangi Verifikasi!", style: grey16w400, textAlign: TextAlign.center),
                    SizedBox(height: setHeight(0.0175)),
                     
                  ],
                )),
              SizedBox(height: setHeight(0.0175)),



              //section 4
              Container(
                alignment: Alignment.bottomCenter,
                child: BaseButtonNextColor(
                  height: setHeight(0.06),
                  textTitle: "Ulangi Verifikasi",
                  colorBotton: orangeColor,
                  colorText: Colors.white,
                  onpressed: (){
                    Navigator.pushNamedAndRemoveUntil(context, VerificationMenuPage.routes, (route) => false, arguments: 1);
                  },
                ),
              ),

              SizedBox(height: setHeight(0.035)),

            ],
          ),
        ),
      )
            ],
          ),
        ),
      ),
    );
  }

}