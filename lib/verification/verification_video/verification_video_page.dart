import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:postpaymobile/utility/theme.dart';
import 'package:postpaymobile/verification/verification_video/verification_video_succes_page.dart';
import 'package:postpaymobile/widget/base_state.dart';

class VerificationVideoPage extends StatefulWidget {
  static const String routes = '/verif_video_page';

  final String from;

  VerificationVideoPage({this.from});
  @override
  _VerificationVideoPageState createState() => _VerificationVideoPageState();
}

class _VerificationVideoPageState extends BaseStatefulState<VerificationVideoPage> with SingleTickerProviderStateMixin {
  CameraController _controller;
  XFile file;
  bool flash = false;
  int cameraFlip = 1;

  AnimationController _animationController;
  Animation _animationChange;

  String message = "Kamu bisa mulai dengan gerakan kecil, ya";


  getGuide() async{
    Future.delayed(Duration(seconds: 1), () async{

      for(int i=1; i <= 3; i++){

        /////////////////////////////////////////////looping pertama start video di detik ke 2
        if(i == 1){
          
          Timer(Duration(seconds: 2), () async {
              await _controller.startVideoRecording();
          });

        /////////////////////////////////////////////looping pertama stop video di detik ke 7, total video 5 detik
        } else if(i == 2){

          Timer(Duration(seconds: 7), () async{ 

            file = await _controller.stopVideoRecording();
            print("File videonya "+file.path.toString());

            setState(() {
              message = "Kamu terverifikasi!\nKamu akan diarahkan ke tahap selanjutnya";
            });
          });


        } else if(i == 3){

          Timer(Duration(seconds: 10), () {
            Navigator.pushNamed(context, VerificationVideoSuccesPage.routes, arguments: file);
          });

        }
      }
    });
  }

  Future<void> initializeCamera() async {
    var cameras = await availableCameras();
    _controller = CameraController(cameras[cameraFlip], ResolutionPreset.medium);
    await _controller.initialize();
  }

  // toChangeCamera(){
  //   setState(() {
  //     if(cameraFlip == 0){
  //       cameraFlip = 1;
  //     } else {
  //       cameraFlip = 0;
  //     }
  //   });
  // }

  toFlash(){
    // setState(() {
      if(flash){
        _controller.setFlashMode(FlashMode.off);
        flash = false;
      } else {
        _controller.setFlashMode(FlashMode.torch);
        flash = true;
      }
    // });
  }

  Future initColors() async {
    while (true) {
      await new Future.delayed(const Duration(milliseconds: 1000), () {
        if (_animationController.status == AnimationStatus.completed) {
          _animationController.reverse();
        } else {
          _animationController.forward();
        }
      });
    }
  }
  
  @override
  void initState() {

    getGuide();

    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 1000));
    _animationChange = ColorTween(begin: Colors.red, end: Colors.white).animate(_animationController);
    initColors();

    super.initState();
  }

  @override
  void dispose() {

    _animationController.dispose();
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        handleDismis();
        await _controller.stopVideoRecording();
        return;
      },
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.black,
          body: FutureBuilder(
            future: initializeCamera(),
            builder: (_, snapshot) =>
            (snapshot.connectionState == ConnectionState.done) ? 
              Stack(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.symmetric(vertical: setHeight(0.015), horizontal: setWidth(0.075)),
                        child: InkWell(
                          onTap: (){
                            toFlash();
                          },
                          child: Icon(Icons.flash_on, color: Colors.white)),
                      ),
                      Stack(
                        children: [
                          Container(
                            height: size.width / _controller.value.aspectRatio,
                            width: size.width,
                            child: CameraPreview(_controller),
                          ),
                          // Image.asset((!message.toString().contains("terverifikasi")) ? 'assets/ic_bg_video.png' : 'assets/ic_bg_video2.png',
                          //   fit: BoxFit.cover,
                          // ),
                          Container(
                            height: setHeight(0.075),
                            color: Colors.grey[700],
                            alignment: Alignment.center,
                            child: Text(message.toString(), style: white14w500, textAlign: TextAlign.center),
                          )
                        ],
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: setHeight(0.2),
                      alignment: Alignment.center,
                      color: Colors.black,
                      padding: EdgeInsets.symmetric(horizontal: setWidth(0.1)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: (){
                              handleDismis();
                            },
                            child: Text("Cancel", style: white16w400)),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("VIDEO", style: yellow12w400),
                              SizedBox(height:5),
                              // InkWell(
                              // onTap: (){
                              //   getTakePicture();
                              // },
                              // child: Image.asset("assets/ic_take.png", scale: 2)),
                              AnimatedBuilder(
                                animation: _animationChange,
                                builder: (context, child) => InkWell(
                                  onTap: (){
                                  },
                                  child: Image.asset("assets/ic_take.png", scale: 2, color: _animationChange.value))),
                              SizedBox(height:setHeight(0.025)),
                            ],
                          ),
                          InkWell(
                            onTap: (){
                              // toChangeCamera();
                            },
                            child: Icon(Icons.flip_camera_ios_outlined, color: Colors.white, size: 40,))
                        ],
                      ))
                  ),
                ],
              )
            : Center(
            child: SizedBox(
              height: 20,
              width: 20,
              child: CircularProgressIndicator(),
            ),
          )),
        ),
      ),
    );
  }

}
