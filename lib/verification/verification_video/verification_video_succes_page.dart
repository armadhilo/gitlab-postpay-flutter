import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:postpaymobile/utility/theme.dart';
import 'package:postpaymobile/verification/verification_video/verification_video_failed_page.dart';
import 'package:postpaymobile/widget/base_button.dart';
import 'package:postpaymobile/widget/base_state.dart';

class VerificationVideoSuccesPage extends StatefulWidget {
  static const String routes = '/verif_video_succes';

  final XFile file;

  VerificationVideoSuccesPage({this.file});

  @override
  State<VerificationVideoSuccesPage> createState() => _VerificationVideoSuccesPageState();
}

class _VerificationVideoSuccesPageState extends BaseStatefulState<VerificationVideoSuccesPage> {
 
  @override
  void initState() {
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        handleDismis();
        handleDismis();
        return;
      },
      child: Scaffold(
        backgroundColor: Colors.grey[100],
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              SizedBox(height: setHeight(0.035)),

              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("assets/ic_succes.png", scale: 2),

                    SizedBox(height: setHeight(0.0175)),
                    Text("Verifikasi Sukses!!", style: black18w700),
                    SizedBox(height: setHeight(0.0175)),
                    Text("Kamu sekarang bisa berselancar dan bayar apa saja di Postpay!", style: grey16w400, textAlign: TextAlign.center),
                    SizedBox(height: setHeight(0.0175)),
                     
                  ],
                )),
              SizedBox(height: setHeight(0.0175)),



              //section 4
              Container(
                alignment: Alignment.bottomCenter,
                child: BaseButtonNextColor(
                  height: setHeight(0.06),
                  textTitle: "Mulai Menjelajah",
                  colorBotton: orangeColor,
                  colorText: Colors.white,
                  onpressed: (){
                    Navigator.pushNamed(context, VerificationVideoFailedPage.routes);
                  },
                ),
              ),

              SizedBox(height: setHeight(0.035)),

            ],
          ),
        ),
      )
            ],
          ),
        ),
      ),
    );
  }

}