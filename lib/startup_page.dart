import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:postpaymobile/login_page.dart';
import 'package:postpaymobile/widget/base_alert.dart';

class StartupPage extends StatefulWidget {
  static const String routes = '/startup';

  @override
  State<StartupPage> createState() => _StartupPageState();
}

class _StartupPageState extends State<StartupPage> {

  Future checkPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.location,
      Permission.camera,
      Permission.storage
    ].request();
    if (statuses[Permission.location].isGranted &&
        statuses[Permission.camera].isGranted &&
        statuses[Permission.storage].isGranted) {
      // print('Izin Disetujui');
      toVerificationMenu();
    } else {
      showAlertMessageSettings(context, "Pesan", "Aplikasi POSTPAY Membutuhkan perijinan Akses : Camera, Penyimpanan. Pergi Ke Settings untuk mengijinkan Permission", "SETTING");
      // print('Izin Ditolak');
    }
  }
  
  @override
  void initState() {
    checkPermission();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            child: Image.asset("assets/ic_icon.png", scale: 2)),
          SizedBox(height:20),
          SpinKitSpinningLines(
            color: Colors.orange,
            size: 50,
          )
        ],
      ),
    );
  }

  void toVerificationMenu(){
    Future.delayed(Duration(milliseconds: 1000), () {
      Navigator.pushNamedAndRemoveUntil(context, LoginPage.routes, (route) => false);
      
      // Navigator.pushNamedAndRemoveUntil(context, VerificationMenuPage.routes, (route) => false, arguments: 1);
    });
  }
}